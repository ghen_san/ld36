﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class _GameManager : MonoBehaviour {

    public Material[] lampMaterial;

    public GameObject[] PlugHole;

    public int[] plugFlag;
    public int[] plugFlag2;

    public string plugUsedName;

    private int randTarget;

    public string TargetStart;
    public string TargetFinal;

    public bool firstTargetPluggedBool;
    public bool finalTargetPluggedBool;

    int counterRinger;

    public GameObject score;
    public int scoreInt;

    /// <================== UI STUFF =======================>
    /// 
    public Text playerText;
    string playerString = "Operator,Number Please";

    public Text botText;
    /// 
    /// <===================== END =========================>

    // Use this for initialization
    void Start ()
    {
        StartCoroutine(CountingTheGameStart());
        scoreInt = 0;
        counterRinger = 0;
        firstTargetPluggedBool = false;
        finalTargetPluggedBool = false;
        plugUsedName = null;
    }

    // VERY FIRST START THE GAME
    IEnumerator CountingTheGameStart()
    {
        yield return new WaitForSeconds(2);
        firstCall();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void firstCall()
    {
        bool RepeatedNum = false;
        randTarget = 0;

        do
        {
            print("run first call repeatedly cause of repeated number");

            RepeatedNum = false;
            randTarget = Random.Range(0, PlugHole.Length);

            for (int j = 0; j <= 6; j++)
            {
                if (randTarget == plugFlag[j])
                {
                    RepeatedNum = true;
                    break;
                }
            }

            for (int k = 0; k <= 6; k++)
            {
                if (randTarget == plugFlag2[k])
                {
                    RepeatedNum = true;
                    break;
                }
            }

        } while (RepeatedNum == true);

        // PUT FLAG SLOT PLUG 1ST USED
        plugFlag[counterRinger] = randTarget;

        PlugHole[randTarget].transform.GetChild(0).GetComponent<MeshRenderer>().material = lampMaterial[1];
        PlugHole[randTarget].transform.GetChild(0).name = "LampOn";

        TargetStart = PlugHole[randTarget].name;

        // COUNTDOWN TIMER
        StartCalltimer();

        //play audio loop caller in
        GetComponent<AudioScript>().playAudioLoop(0);
    }

    public void firstTargetDone()
    {
        PlugHole[randTarget].transform.GetChild(0).GetComponent<MeshRenderer>().material = lampMaterial[0];
        PlugHole[randTarget].transform.GetChild(0).name = "LampOff";

        transform.GetComponent<CallTimer>().stopUpperCountdown();

        playerTextType();
    }

    public void secondCall()
    {
        bool RepeatedNum = false;
        randTarget = 0;

        do
        {
            print("run second call repeatedly cause of repeated number");

            RepeatedNum = false;
            randTarget = Random.Range(0, PlugHole.Length);

            for (int j = 0; j <= 6; j++)
            {
                if (randTarget == plugFlag[j])
                {
                    RepeatedNum = true;
                    break;
                }
            }

            for (int k = 0; k <= 6; k++)
            {
                if (randTarget == plugFlag2[k])
                {
                    RepeatedNum = true;
                    break;
                }
            }

        } while (RepeatedNum  == true);

        TargetFinal = PlugHole[randTarget].name;

        // PUT FLAG SLOT PLUG 1ST USED
        plugFlag2[counterRinger] = randTarget;

        string word = PlugHole[randTarget].name;

        botText.transform.parent.gameObject.SetActive(true);

        StartCoroutine(botTextAnim(word));

        if (counterRinger >= 6)
        {
            counterRinger = 0;
        }
        else
        {
            counterRinger++;
        }
    }

    IEnumerator waitForNextCall()
    {
       
        yield return new WaitForSeconds(_StaticVal.nextCallTime);
        firstCall();
    }

    public void playerTextType()
    {
        playerText.transform.parent.gameObject.SetActive(true);
        StartCoroutine(PlayerTextAnim());
    }

    IEnumerator PlayerTextAnim()
    {
        foreach (char c in playerString)
        {
            playerText.text += c;

            GetComponent<AudioScript>().playAudioOnce(1, 0.173f);

            yield return new WaitForSeconds(0.125f);
        }

        secondCall();
    }

    IEnumerator botTextAnim(string word)
    {
        foreach (char c in word)
        {
            botText.text += c;

            GetComponent<AudioScript>().playAudioOnce(1, 0.173f);

            yield return new WaitForSeconds(0.125f);
        }

        StartCalltimer();
    }

    public void StartCalltimer()
    {
        // COUNTDOWN TIMER
        transform.GetComponent<CallTimer>().CountdownTimerHealth(_StaticVal.upperWaitingTime);
    }

    //CALL FINISH !
    public void RestartTheCall()
    {
        playerText.transform.parent.gameObject.SetActive(false);
        botText.transform.parent.gameObject.SetActive(false);
        firstTargetPluggedBool = false;
        finalTargetPluggedBool = false;
        plugUsedName = null;

        playerText.text = "";
        botText.text = "";

        //ADD SCORE
        GetComponent<_GameManager>().AddScore("finishCall");
        //END

        transform.GetComponent<CallTimer>().stopUpperCountdown();
        StartCoroutine(waitForNextCall());

        //stop the audio
        GetComponent<AudioScript>().stopAudioLoop();
    }


    // SCORING SYSTEM
    public void AddScore(string type)
    {
        if (type == "finishCall")
        {
            scoreInt += (4 * 3);
        }

        if (type == "reset")
        {
            scoreInt += (7 * 1);
        }

        score.GetComponent<TextMesh>().text = scoreInt.ToString();
        ScoreProgressSystem(scoreInt);
    }

    private void ScoreProgressSystem(int val)
    {
        if (val > 60 && val < 110)
        {
            _StaticVal.upperWaitingTime = 8;
            _StaticVal.minCallingTime = 30;
        }
        if (val > 120 && val < 150)
        {
            _StaticVal.resetWaitingTime = 8;
            _StaticVal.minCallingTime = 25;
            _StaticVal.maxCallingTime = 35;
        }

        if (val > 150 && val < 190)
        {
            _StaticVal.nextCallTime = 1;
        }

        if (val > 190 && val < 230)
        {
            _StaticVal.upperWaitingTime = 5;
            _StaticVal.resetWaitingTime = 5;
        }

        if (val > 230 && val < 250)
        {
            _StaticVal.minCallingTime = 15;
            _StaticVal.maxCallingTime = 18;
        }

        if (val > 450)
        {
            _StaticVal.upperWaitingTime = 3;
            _StaticVal.resetWaitingTime = 2;
        }
    }
}
