﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

    public GameObject[] Tutorial;
    public GameObject TutorialPanel;
    public GameObject creditPanel;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PlayGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }

    public void backToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void toWebsite()
    {
        Application.OpenURL("http://bagus.kurangtidur.com");
    }

    public void TutorialOpenAndClose()
    {
        if (TutorialPanel.activeInHierarchy == false)
        {
            TutorialPanel.SetActive(true);
        }
        else
        {
            TutorialPanel.SetActive(false);
        }
    }

    public void CreditOpenAndClose()
    {
        if (creditPanel.activeInHierarchy == false)
        {
            creditPanel.SetActive(true);
        }
        else
        {
            creditPanel.SetActive(false);
        }
    }

    public void TutorialNext(int val)
    {
        if(val == 0)
        {
            Tutorial[1].SetActive(true);
        }
        if (val == 1)
        {
            Tutorial[2].SetActive(true);
        }
        if (val == 2)
        {
            Tutorial[1].SetActive(false);
            Tutorial[2].SetActive(false);
            TutorialOpenAndClose();
        }
    }
}
