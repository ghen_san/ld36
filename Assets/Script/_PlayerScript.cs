﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class _PlayerScript : MonoBehaviour {

    public GameObject[] HealthSprite;
    public int healthInt;

    public GameObject gameoverPanel;
    public Text ScoreText;
	// Use this for initialization
	void Start ()
    {
        healthInt = 3;
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void ReduceHealth()
    {
        healthInt--;
        GetComponent<AudioScript>().playAudioOnce(5,1f);


        if (healthInt <= 0)
        {
            GameOver();
            Time.timeScale = 0;
            GetComponent<AudioScript>().stopAudioLoop();
        }

        HealthSprite[healthInt].SetActive(false);
    }

    public void GameOver()
    {
        gameoverPanel.SetActive(true);

        ScoreText.text = GetComponent<_GameManager>().scoreInt.ToString();
    }
}
