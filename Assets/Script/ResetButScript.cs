﻿using UnityEngine;
using System.Collections;

public class ResetButScript : MonoBehaviour {

    public GameObject gamemananger;

	// Use this for initialization
	void Start ()
    {
        gamemananger = GameObject.Find("GAMEMANAGER_OBJ");
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown()
    {
        if (gamemananger.GetComponent<CallTimer>().PanelStatus[int.Parse(transform.parent.GetChild(0).name) - 1] == false && transform.parent.GetChild(0).GetComponent<PlugScript>().CallerAlreadyMove == false && transform.parent.GetChild(1).GetComponent<PlugScript>().ReceiverAlreadyMove == false && transform.parent.GetChild(0).GetComponent<PlugScript>().Resetable == true)
        {
            transform.parent.GetChild(0).GetComponent<PlugScript>().ResetPlug();
            transform.parent.GetChild(1).GetComponent<PlugScript>().ResetPlug();
            transform.parent.GetChild(2).GetChild(0).GetComponent<MeshRenderer>().material = gamemananger.GetComponent<_GameManager>().lampMaterial[0];
            transform.parent.GetChild(2).GetChild(1).GetComponent<MeshRenderer>().material = gamemananger.GetComponent<_GameManager>().lampMaterial[0];

            transform.parent.GetChild(0).GetComponent<PlugScript>().Resetable = false;

            gamemananger.GetComponent<CallTimer>().stopResetButtonCountdown(int.Parse(transform.parent.GetChild(0).name) - 1);
            transform.parent.transform.GetChild(4).GetComponent<TextMesh>().text = "10";
            transform.parent.transform.GetChild(4).gameObject.SetActive(false);

            gamemananger.GetComponent<_GameManager>().AddScore("reset");

            gamemananger.GetComponent<AudioScript>().playAudioOnce(4, 5f);

            print("Sukses Reset");
        }
        else
        {
            print("Reset Gagal");
        }
    }

    public void ForceReset()
    {
        transform.parent.GetChild(0).GetComponent<PlugScript>().ResetPlug();
        transform.parent.GetChild(1).GetComponent<PlugScript>().ResetPlug();
        transform.parent.GetChild(2).GetChild(0).GetComponent<MeshRenderer>().material = gamemananger.GetComponent<_GameManager>().lampMaterial[0];
        transform.parent.GetChild(2).GetChild(1).GetComponent<MeshRenderer>().material = gamemananger.GetComponent<_GameManager>().lampMaterial[0];

        gamemananger.GetComponent<CallTimer>().stopResetButtonCountdown(int.Parse(transform.parent.GetChild(0).name) - 1);
        transform.parent.transform.GetChild(4).GetComponent<TextMesh>().text = "10";
        transform.parent.transform.GetChild(4).gameObject.SetActive(false);

        gamemananger.GetComponent<AudioScript>().playAudioOnce(4, 5f);
    }
}
