﻿using UnityEngine;
using System.Collections;

public class _StaticVal : MonoBehaviour {

    public static int upperWaitingTime;
    public static int resetWaitingTime;
    public static int nextCallTime;

    public static int minCallingTime;
    public static int maxCallingTime;


    // Use this for initialization
    void Awake ()
    {
        upperWaitingTime = 10;
        resetWaitingTime = 10;
        nextCallTime = 3;
        minCallingTime = 35;
        maxCallingTime = 40;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
