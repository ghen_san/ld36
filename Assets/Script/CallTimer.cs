﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CallTimer : MonoBehaviour {

    public GameObject[] PanelPlug;
    public bool[] PanelStatus;

    public GameObject timer;

    public GameObject PlugPlace;

    Coroutine countdownCouroutineRef = null;

    Coroutine[] countdownResetButtonRef = new Coroutine[7];

    // Use this for initialization
    void Start ()
    {
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StatusCalling(int index)
    {
        PanelStatus[index] = true;
        PanelPlug[index].transform.GetChild(2).GetChild(0).GetComponent<MeshRenderer>().material = GetComponent<_GameManager>().lampMaterial[1];

        StartCoroutine(TimerCalling(index));
    }

    IEnumerator TimerCalling(int index)
    {
        int rand = Random.Range(_StaticVal.minCallingTime,_StaticVal.maxCallingTime);
        yield return new WaitForSeconds(rand);
        DoneCalling(index);
    }

    public void DoneCalling(int index)
    {
        PanelPlug[index].transform.GetChild(2).GetChild(0).GetComponent<MeshRenderer>().material = GetComponent<_GameManager>().lampMaterial[0];
        PanelPlug[index].transform.GetChild(2).GetChild(1).GetComponent<MeshRenderer>().material = GetComponent<_GameManager>().lampMaterial[1];

        PanelPlug[index].transform.GetChild(0).GetComponent<PlugScript>().CallerAlreadyMove = false;
        PanelPlug[index].transform.GetChild(1).GetComponent<PlugScript>().ReceiverAlreadyMove = false;

        PanelPlug[index].transform.GetChild(0).GetComponent<PlugScript>().Resetable = true;

        PanelPlug[index].transform.GetChild(4).gameObject.SetActive(true);

        CountdownResetButton(_StaticVal.resetWaitingTime, index);

        PanelStatus[index] = false;
    }

    //=================================== RESET BUTTON COUNTDOWN ===================================

    public void CountdownResetButton(int val, int index)
    {
        if (index == 0)
        {
            countdownResetButtonRef[0] = StartCoroutine(CountdownResetButtonIenumeratorIndex0(val, index));
        }
        if (index == 1)
        {
            countdownResetButtonRef[1] = StartCoroutine(CountdownResetButtonIenumeratorIndex1(val, index));
        }
        if (index == 2)
        {
            countdownResetButtonRef[2] = StartCoroutine(CountdownResetButtonIenumeratorIndex2(val, index));
        }
        if (index == 3)
        {
            countdownResetButtonRef[3] = StartCoroutine(CountdownResetButtonIenumeratorIndex3(val, index));
        }
        if (index == 4)
        {
            countdownResetButtonRef[4] = StartCoroutine(CountdownResetButtonIenumeratorIndex4(val, index));
        }
        if (index == 5)
        {
            countdownResetButtonRef[5] = StartCoroutine(CountdownResetButtonIenumeratorIndex5(val, index));
        }

        if (index == 6)
        {
            countdownResetButtonRef[6] = StartCoroutine(CountdownResetButtonIenumeratorIndex6(val, index));
        }
    }

    // THIS IS A STUPIDITY

    private IEnumerator CountdownResetButtonIenumeratorIndex0(int time, int index)
    {
        while (time > 0)
        {
            PanelPlug[0].transform.GetChild(4).GetComponent<TextMesh>().text = time.ToString();
            yield return new WaitForSeconds(1);
            time--;
        }

        transform.GetComponent<_PlayerScript>().ReduceHealth();
        stopResetButtonCountdown(0);
        forceResetButton(0);

        Debug.Log("Countdown Complete!");
    }

    private IEnumerator CountdownResetButtonIenumeratorIndex1(int time, int index)
    {
        while (time > 0)
        {
            PanelPlug[1].transform.GetChild(4).GetComponent<TextMesh>().text = time.ToString();
            yield return new WaitForSeconds(1);
            time--;
        }

        transform.GetComponent<_PlayerScript>().ReduceHealth();
        stopResetButtonCountdown(1);
        forceResetButton(1);

        Debug.Log("Countdown Complete!");
    }

    private IEnumerator CountdownResetButtonIenumeratorIndex2(int time, int index)
    {
        while (time > 0)
        {
            PanelPlug[2].transform.GetChild(4).GetComponent<TextMesh>().text = time.ToString();
            yield return new WaitForSeconds(1);
            time--;
        }

        transform.GetComponent<_PlayerScript>().ReduceHealth();
        stopResetButtonCountdown(2);
        forceResetButton(2);

        Debug.Log("Countdown Complete!");
    }

    private IEnumerator CountdownResetButtonIenumeratorIndex3(int time, int index)
    {
        while (time > 0)
        {
            PanelPlug[3].transform.GetChild(4).GetComponent<TextMesh>().text = time.ToString();
            yield return new WaitForSeconds(1);
            time--;
        }

        transform.GetComponent<_PlayerScript>().ReduceHealth();
        stopResetButtonCountdown(3);
        forceResetButton(3);
        Debug.Log("Countdown Complete!");
    }

    private IEnumerator CountdownResetButtonIenumeratorIndex4(int time, int index)
    {
        while (time > 0)
        {
            PanelPlug[4].transform.GetChild(4).GetComponent<TextMesh>().text = time.ToString();
            yield return new WaitForSeconds(1);
            time--;
        }

        transform.GetComponent<_PlayerScript>().ReduceHealth();
        stopResetButtonCountdown(4);
        forceResetButton(4);

        Debug.Log("Countdown Complete!");
    }

    private IEnumerator CountdownResetButtonIenumeratorIndex5(int time, int index)
    {
        while (time > 0)
        {
            PanelPlug[5].transform.GetChild(4).GetComponent<TextMesh>().text = time.ToString();
            yield return new WaitForSeconds(1);
            time--;
        }

        transform.GetComponent<_PlayerScript>().ReduceHealth();
        stopResetButtonCountdown(5);
        forceResetButton(5);

        Debug.Log("Countdown Complete!");
    }

    private IEnumerator CountdownResetButtonIenumeratorIndex6(int time, int index)
    {
        while (time > 0)
        {
            PanelPlug[6].transform.GetChild(4).GetComponent<TextMesh>().text = time.ToString();
            yield return new WaitForSeconds(1);
            time--;
        }

        transform.GetComponent<_PlayerScript>().ReduceHealth();
        stopResetButtonCountdown(6);
        forceResetButton(6);

        Debug.Log("Countdown Complete!");
    }

    // THE END OF A STUPIDITY

    // STOP BUTTON COUNTDOWN
    public void stopResetButtonCountdown(int index)
    {
        StopCoroutine(countdownResetButtonRef[index]);
    }

    // FORCE RESTART
    public void forceResetButton(int index)
    {
        PlugPlace.transform.GetChild(index).GetChild(3).GetComponent<ResetButScript>().ForceReset();
    }

    // ================================== END OF RESET BUT COUNTDOWN ============================================

    // UPPER COUNTDOWN
    public void stopUpperCountdown()
    {
        StopCoroutine(countdownCouroutineRef);
        timer.GetComponent<TextMesh>().text = _StaticVal.upperWaitingTime.ToString();
    }

    public void CountdownTimerHealth(int val)
    {
        countdownCouroutineRef = StartCoroutine(Countdown(val));
    }

    public IEnumerator Countdown(int time)
    {
        while (time > 0)
        {
            timer.GetComponent<TextMesh>().text = time.ToString();
            yield return new WaitForSeconds(1);
            time--;
        }

        // REDUCE HEALTH AND RESET TIMER
        transform.GetComponent<_PlayerScript>().ReduceHealth();
        stopUpperCountdown();
        CountdownTimerHealth(_StaticVal.upperWaitingTime);

        //Debug.Log("Countdown Complete!");
    }
}
