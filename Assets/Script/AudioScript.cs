﻿using UnityEngine;
using System.Collections;

public class AudioScript : MonoBehaviour {

    public AudioClip[] ListAudio;
    public AudioSource audio;
    public AudioSource audio2;

    // Use this for initialization
    void Start ()
    {
       
    }

    public void playAudioLoop(int val)
    {
        audio.Stop();
        audio.loop = true;
        audio.clip = ListAudio[val];
        audio.Play();
    }

    public void stopAudioLoop()
    {
        audio.Stop();
    }

    public void playAudioOnce(int val, float volume)
    {
        audio2.PlayOneShot(ListAudio[val], volume);
    }

    // Update is called once per frame
    void Update ()
    {
	
	}
}
