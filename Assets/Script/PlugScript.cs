﻿using UnityEngine;
using System.Collections;

public class PlugScript : MonoBehaviour {

    private bool clicked;

    public bool CallerAlreadyMove;
    public bool ReceiverAlreadyMove;
    public bool Resetable;

    GameObject gamemanager;

    private Vector3 plugOriginalLocation;

    public bool touchTargetPlug;

    private Vector3 targetPlugLocation;
    private Vector3 targetPlugLocation2;

    private int clickCounter;

    // Use this for initialization
    void Start ()
    {
        CallerAlreadyMove = false;
        ReceiverAlreadyMove = false;
        Resetable = false;
        clicked = false;
        touchTargetPlug = false;
        plugOriginalLocation = this.transform.position;

        gameObject.GetComponent<LineRenderer>().SetPosition(0, this.transform.position);
        gameObject.GetComponent<LineRenderer>().SetPosition(1, this.transform.position);

        gamemanager = GameObject.Find("GAMEMANAGER_OBJ");

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (clicked == true)
        {
            Vector3 temp = Input.mousePosition;
            temp.z = 10f; // Set this to be the distance you want the object to be placed in front of the camera.
            this.transform.position = Camera.main.ScreenToWorldPoint(temp);

            gameObject.GetComponent<LineRenderer>().SetPosition(1, this.transform.position);

            this.transform.GetChild(0).gameObject.SetActive(false);
            this.transform.GetChild(1).gameObject.SetActive(true);
        }
        else
        { }
	}

    void OnMouseDown()
    {
        clickCounter++;

        if (clicked == false)
        {
            if (gameObject.tag == "CallerPlug")
            {
                if (gameObject.tag == "CallerPlug" && gamemanager.GetComponent<_GameManager>().firstTargetPluggedBool == false)
                {
                    if (clicked == false)
                    {
                        clicked = true;
                    }

                    print(gameObject.name + " " + gameObject.tag + " OK");
                    gamemanager.GetComponent<AudioScript>().playAudioOnce(2, 0.173f);
                }
                else
                {
                    print(gameObject.name + " " + gameObject.tag + " NOPE");
                    clickCounter = 0;
                }
            }


            if (gameObject.tag == "ReceiverPlug")
            {
                if (gameObject.tag == "ReceiverPlug" && gamemanager.GetComponent<_GameManager>().firstTargetPluggedBool == true && gameObject.name == gamemanager.GetComponent<_GameManager>().plugUsedName)
                {
                    if (clicked == false)
                    {
                        clicked = true;
                    }

                    print(gameObject.name + " " + gameObject.tag + " OK");
                    gamemanager.GetComponent<AudioScript>().playAudioOnce(2, 0.173f);
                }
                else
                {
                    print(gameObject.name + " " + gameObject.tag + " NOPE");
                    clickCounter = 0;
                }
            }
        }

        // REAL PLUG BEHAVIOUR
        if (clicked == true && clickCounter == 2)
        {
            if ( gameObject.tag == "CallerPlug")
            {
                if (touchTargetPlug == true)
                {
                    gamemanager.GetComponent<_GameManager>().firstTargetPluggedBool = true;

                    this.transform.position = targetPlugLocation;
                    gameObject.GetComponent<LineRenderer>().SetPosition(1, targetPlugLocation);
                    clicked = false;
                    touchTargetPlug = false;
                    clickCounter = 0;
                    CallerAlreadyMove = true;
                    gamemanager.GetComponent<_GameManager>().firstTargetDone();
                    gamemanager.GetComponent<_GameManager>().plugUsedName = gameObject.name;

                    gamemanager.GetComponent<AudioScript>().playAudioOnce(3, 0.173f);

                    this.transform.GetChild(0).gameObject.SetActive(false);
                    this.transform.GetChild(1).gameObject.SetActive(true);
                }
                else if (touchTargetPlug == false)
                {
                    print("test");

                    this.transform.position = plugOriginalLocation;
                    gameObject.GetComponent<LineRenderer>().SetPosition(1, plugOriginalLocation);
                    clicked = false;
                    touchTargetPlug = false;
                    clickCounter = 0;

                    this.transform.GetChild(0).gameObject.SetActive(true);
                    this.transform.GetChild(1).gameObject.SetActive(false);
                }
            }

            // RECEIVER PLUG VERIF

            if (gameObject.tag == "ReceiverPlug")
            {
                if (touchTargetPlug == true)
                {
                    gamemanager.GetComponent<_GameManager>().finalTargetPluggedBool = true;

                    this.transform.position = targetPlugLocation;
                    gameObject.GetComponent<LineRenderer>().SetPosition(1, targetPlugLocation);
                    clicked = false;
                    touchTargetPlug = false;
                    clickCounter = 0;

                    ReceiverAlreadyMove = true;
                    //transform.parent.GetChild(0).GetComponent<PlugScript>().CallerAlreadyMove = false;

                    this.transform.GetChild(0).gameObject.SetActive(false);
                    this.transform.GetChild(1).gameObject.SetActive(true);

                    gamemanager.GetComponent<AudioScript>().playAudioOnce(3, 0.173f);

                    gamemanager.GetComponent<CallTimer>().StatusCalling(int.Parse(this.name) - 1);

                    //END CALL CYCLE ACTIVITY
                    finish1Call();
                }
                else if (touchTargetPlug == false)
                {
                    print("test");

                    this.transform.position = plugOriginalLocation;
                    gameObject.GetComponent<LineRenderer>().SetPosition(1, plugOriginalLocation);
                    clicked = false;
                    touchTargetPlug = false;
                    clickCounter = 0;

                    this.transform.GetChild(0).gameObject.SetActive(true);
                    this.transform.GetChild(1).gameObject.SetActive(false);
                }
            }            
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlugTarget")
        {
            if (other.transform.name == gamemanager.GetComponent<_GameManager>().TargetStart && gameObject.tag == "CallerPlug")
            {
                touchTargetPlug = true;
                targetPlugLocation = other.transform.position;
            }

            if (other.transform.name == gamemanager.GetComponent<_GameManager>().TargetFinal && gameObject.tag == "ReceiverPlug")
            {
                touchTargetPlug = true;
                targetPlugLocation = other.transform.position;
            }

        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "PlugTarget")
        {
            if (other.transform.name == gamemanager.GetComponent<_GameManager>().TargetStart && gameObject.tag == "CallerPlug")
            {
                touchTargetPlug = false;
                //targetPlugLocation = other.transform.position;
            }

            if (other.transform.name == gamemanager.GetComponent<_GameManager>().TargetFinal && gameObject.tag == "ReceiverPlug")
            {
                touchTargetPlug = false;
                //targetPlugLocation = other.transform.position;
            }
        }

    }

    public void finish1Call()
    {
        gamemanager.GetComponent<_GameManager>().RestartTheCall();
    }

    public void ResetPlug()
    {
        this.transform.position = plugOriginalLocation;
        gameObject.GetComponent<LineRenderer>().SetPosition(1, plugOriginalLocation);
        clicked = false;
        touchTargetPlug = false;
        clickCounter = 0;

        this.transform.GetChild(0).gameObject.SetActive(true);
        this.transform.GetChild(1).gameObject.SetActive(false);

        print("RESET : " + gameObject.name);

    }
}
